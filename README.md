# DNS Zone Builder

The zone builder script is designed to build DNS zones from templates.

The path option in the [source] section of the config file determines the base, where zones are found, with templates located in a `tmpl` subdirectory.

The `tmpl` subdirectory MUST contain a `soa` file, and that file is expected to have two replacable variables `__ZONE__` and `__SERIAL__`

An example is as follows:

~~~
$TTL 1D
$ORIGIN __ZONE__.

@	IN	SOA	ns.example.com. hostmaster.example.com. (
			__SERIAL__	; Serial
			14400		; Slave Refresh (4 hours)
			900		; Slave Retry in the event of error (15 mins)
			2419200		; Slave Expire (4 weeks)
			900		; Minimum cache time in case of failed lookups (15 mins)
		)
~~~

The zone file templates are basically normal DNS zone files, but without the SOA, added by the script from the template.

However, you can include files from the `tmpl` directory.

For example, lets say I want a consistent set of NS records in every zone, I can include a common template for that:

~~~
__I_my_nameservers
~~~

And then in `tmpl/my_nameservers` you would have your normal NS lines:

~~~
@	NS	ns1.example.com.
	NS	ns2.example.com.
~~~

The code requires the ISC python module that ships with BIND 9.11;

The code is written to use catalog zones, which it updates and checks with dynamic updates and it adds or removes zones from the master with RNDC commands.

It expects to use TSIG keys for both of the above. I may add support for dynamic updates without keys at some future point.

## Catalog Zones ##

You just need to master a normal looking zone, that has some special content in it.

Mine looks like this, before I add any zones to it:

~~~
$ORIGIN .
$TTL 3600	; 1 hour
catalog			IN SOA	ns.example.com. hostmaster.example.com. (
				1506337813 ; serial
				14400      ; refresh (4 hours)
				900        ; retry (15 minutes)
				2419200    ; expire (4 weeks)
				900        ; minimum (15 minutes)
				)
			NS	ns.example.com.
$ORIGIN catalog.
masters			A	192.168.53.53
			AAAA	2001:db8:53::53
version			TXT	"1"
$ORIGIN zones.catalog.
~~~

There are many more things you can add in here. Take a look [in this ISC knowledgebase article](https://kb.isc.org/article/AA-01401/0/A-short-introduction-to-Catalog-Zones.html)

## DNSSEC ##

The code adds zones in a way that permits signing for my set up, currently. I'll pull this out into the config file at some point, so you can sign elsewhere downstream.

## RNDC ##

The easiest way to do this, is to `rndc-confgen -a -A hmac-sha512 -b 512` and remove RNDC config from named.conf along with rndc.conf.
Your config may be more complicated than this, so then you're on your own ;-)
Suffice to say, your config file needs the algorithm and secret in the relevant places.


## TODO ##
* Option to build a static file for the master to include, instead of using RNDC addzone/delzone/zoneinfo..?
* Option to build a static file for the slave(s) to obtain and include, instead of updating a catalog zone
* Pull signing config out into config file